# Cải tiến phần mềm explorer

##Họ tên: Nguyễn Xuân Tân     MSSV: 1512492


###Tính năng đã làm được:

1. Cải tiến cho phép khi thay đổi kích thước treeview thì listview thay đổi theo

2. Bổ sung Statusbar khi click vào một tập tin trong listview thì hiển thị kích thước tập tin tương ứng, thể hiện dạng động x.x KB/MB/GB/TB/PB

3. Lưu lại kích thước cửa sổ màn hình chính và nạp lại khi chương trình chạy lên


####Link bitbucket: https://bitbucket.org/bond-/1512492_tuan5
####Link youtube: https://youtu.be/-h6KJqKHjs0